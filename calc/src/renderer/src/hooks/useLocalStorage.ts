import LocalStorage from '../lib/LocalStorage';
import { useEffect, useState } from 'react';;

function useLocalStorage<T>(key: string, initialValue: T) {
  // State to store our value
  // Pass initial state function to useState so logic is only executed once
  const [storedValue, setStoredValue] = useState<T>(() => {
    // Get from local storage by key
    return LocalStorage.getItem(key, initialValue);
  });

  // Return a wrapped version of useState's setter function that ...
  // ... persists the new value to localStorage.
  const setValue = (value: T | ((val: T) => T)) => {
    try {
      // Allow value to be a function so we have same API as useState
      const valueToStore = value instanceof Function ? value(storedValue) : value;
      // Save to local storage
      LocalStorage.setItem(key, valueToStore);
      // Save state
      setStoredValue(valueToStore);
    } catch (error) {
      // A more advanced implementation would handle the error case
      console.log(error);
    }
  };

  useEffect(() => {
    LocalStorage.listen(key, (v: T) => {
      setStoredValue(v);
    });
  }, [key]);

  //
  return [storedValue, setValue] as const;
}

//
export default useLocalStorage;
