import { useState, useEffect } from 'react';

import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';

import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';

import FormGroup from '@mui/material/FormGroup';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import InputLabel from '@mui/material/InputLabel';
import TextField from '@mui/material/TextField';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';

import NumericField from '../components/NumericField';

import { assetKinds, AssetKindType, liabilityKinds, LiabilityKindType, LiabilityType, AssetType } from '../interfaces/AssetLiability'

const interestLabel = (kind: string) => {
  switch (kind) {
    case 'Brokerage':
    case '401K':
    case 'IRA':
    case 'Roth':
    case 'HSA':
      return 'Cash Yield';
    case 'Asset':
    case 'Property':
      return 'Appreciation Rate';
    default:
      return 'Interest Rate';
  }
};

const balanceLabel = (kind: string) => {
  switch (kind) {
    case 'Brokerage':
    case '401K':
    case 'IRA':
    case 'Roth':
    case 'HSA':
      return 'Cash plus Investments';
    case 'Property':
      return 'Property Value';
    case 'Asset':
      return 'Asset Value';
    case 'Mortgage':
      return 'Mortgage Balance';
    case 'Loan':
      return 'Loan Balance';
    default:
      return 'Account Balance';
  }

};

/**
 *
 */
interface AssetDialogProps {
  show: boolean;
  asset?: AssetType;
  onClose: Function;
};
const AssetDialog = ({ show, asset: account, onClose }: AssetDialogProps) => {
  const [kind, setKind] = useState<AssetKindType>(account?.kind || 'Generic Asset');
  const [institution, setInstitution] = useState<string>(account?.institution || '');
  const [description, setDescription] = useState<string>(account?.description || '');
  const [balance, setBalance] = useState<number | string>(account?.balance || 0);
  const [interestRate, setInterestRate] = useState<number | string>(account?.interestRate || 0);
  const [error, setError] = useState<string[]>([]);

  useEffect(() => {
    if (account) {
      setKind(account.kind);
      setInstitution(account.institution);
      setDescription(account.description);
      setBalance(account.balance);
      setInterestRate(account.interestRate);
      return;
    }
    setDescription('');
    setBalance('');
    setInterestRate('');

  }, [account]);

  useEffect(() => {
    const newError: string[] = [];

    if (institution == '') {
      newError.push('Institution is a required field');
    }
    if (description == '') {
      newError.push('Description is a required field');
    }
    setError(newError);
  }, [institution, description])

  let key = 0;
  return (
    <Dialog
      open={show}
      onClose={(reason: any) => {
        console.log('reason', reason)
      }}
    >
      <DialogTitle>{account != null ? 'Edit Asset' : 'Add Asset'}</DialogTitle>
      <DialogContent>
        <Paper sx={{ padding: 2 }}>

          <Box
            component="form"
            sx={{
              '& > :not(style)': { m: 1, width: '25ch' },
            }}
            noValidate
            autoComplete="off"
          >
            <FormGroup>

              <Stack direction="column" spacing={2}>

                <FormControl fullWidth>
                  <InputLabel id="asset-select-label">Asset Type</InputLabel>
                  <Select
                    value={kind}
                    label="Asset Type"
                    onChange={(event: SelectChangeEvent) => {
                      setKind(event.target.value as AssetKindType);
                    }}
                  >
                    {assetKinds.map((s: string) => {
                      return <MenuItem key={++key} value={s}>{s}</MenuItem>;
                    })}
                  </Select>
                </FormControl>

                <TextField
                  label="Instutition/Bank"
                  error={institution.length === 0}
                  helperText="Bank or brokerage name"
                  value={institution}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    setInstitution(event.target.value);
                  }}
                />

                <TextField
                  label="Account Description"
                  error={description.length === 0}
                  helperText="Description/Account Number"
                  value={description}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    setDescription(event.target.value);
                  }}
                />

                <NumericField
                  label={balanceLabel(kind)}
                  error={typeof balance === 'string'}
                  helperText={balanceLabel(kind)}
                  value={balance}
                  onChange={(v: string) => {
                    setBalance(v.length ? Number(v) : '')
                  }}
                />

                <NumericField
                  label={interestLabel(kind)}
                  error={typeof interestRate === 'string'}
                  helperText={interestLabel(kind)}
                  value={interestRate}
                  onChange={(v: string) => {
                    setInterestRate(v.length ? Number(v) : '')
                  }}
                />
              </Stack>
            </FormGroup>
            <FormControlLabel control={<Checkbox />} label="Closed/Paid Off" />
          </Box>
        </Paper>
      </DialogContent>
      <DialogActions>
        <Button
          disabled={error.length !== 0}
          onClick={() => {
            onClose({
              kind: kind,
              asset: true,
              institution: institution,
              description: description,
              balance: balance,
              interestRate: interestRate,
            })
          }}
        >
          Add
        </Button>
        <Button
          onClick={() => {
            onClose(null);
          }}
        >
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
};

/**
 *
 * @param param0
 * @returns
 */
interface LiabilityDialogProps {
  show: boolean;
  liability?: LiabilityType;
  onClose: Function;
};
const LiabilityDialog = ({ show, liability, onClose }: LiabilityDialogProps) => {
  const [kind, setKind] = useState<LiabilityKindType>(liability?.kind as LiabilityKindType || 'Generic Liability');
  const [institution, setInstitution] = useState<string>(liability?.institution || '');
  const [description, setDescription] = useState<string>(liability?.description || '');
  const [balance, setBalance] = useState<number | string>(liability?.balance || 0);
  const [interestRate, setInterestRate] = useState<number | string>(liability?.interestRate || 0);
  const [error, setError] = useState<string[]>([]);

  useEffect(() => {
    if (liability) {
      setKind(liability.kind as LiabilityKindType);
      setInstitution(liability.institution);
      setDescription(liability.description);
      setBalance(liability.balance);
      setInterestRate(liability.interestRate);
      return;
    }
    setDescription('');
    setBalance('');
    setInterestRate('');

  }, [liability]);

  useEffect(() => {
    const newError: string[] = [];

    if (institution == '') {
      newError.push('Institution is a required field');
    }
    if (description == '') {
      newError.push('Description is a required field');
    }
    setError(newError);
  }, [institution, description])

  let key = 0;
  return (
    <Dialog
      open={show}
      onClose={(reason: any) => {
        console.log('reason', reason)
      }}
    >
      <DialogTitle>{liability != null ? 'Edit Liability' : 'Add Liability'}</DialogTitle>
      <DialogContent>
        <Paper sx={{ padding: 2 }}>

          <Box
            component="form"
            sx={{
              '& > :not(style)': { m: 1, width: '25ch' },
            }}
            noValidate
            autoComplete="off"
          >
            <FormGroup>

              <Stack direction="column" spacing={2}>

                <FormControl fullWidth>
                  <InputLabel id="liability-dialog-kind-select-label">Liability Type</InputLabel>
                  <Select
                    value={kind}
                    label="Liability Type"
                    onChange={(event: SelectChangeEvent) => {
                      setKind(event.target.value as LiabilityKindType);
                    }}
                  >
                    {liabilityKinds.map((s: string) => {
                      return <MenuItem key={++key} value={s}>{s}</MenuItem>;
                    })}
                  </Select>
                </FormControl>

                <TextField
                  label="Instutition/Bank"
                  error={institution.length === 0}
                  helperText="Bank or brokerage name"
                  value={institution}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    setInstitution(event.target.value);
                  }}
                />

                <TextField
                  label="Account Description"
                  error={description.length === 0}
                  helperText="Description/Account Number"
                  value={description}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    setDescription(event.target.value);
                  }}
                />

                <NumericField
                  label={balanceLabel(kind)}
                  error={typeof balance === 'string'}
                  helperText={balanceLabel(kind)}
                  value={balance}
                  onChange={(v: string) => {
                    setBalance(v.length ? Number(v) : '')
                  }}
                />

                <NumericField
                  label={interestLabel(kind)}
                  error={typeof interestRate === 'string'}
                  helperText={interestLabel(kind)}
                  value={interestRate}
                  onChange={(v: string) => {
                    setInterestRate(v.length ? Number(v) : '')
                  }}
                />
              </Stack>
            </FormGroup>
            <FormControlLabel control={<Checkbox />} label="Closed/Paid Off" />
          </Box>
        </Paper>
      </DialogContent>

      <DialogActions>
        <Button
          disabled={error.length !== 0}
          onClick={() => {
            onClose({
              kind: kind,
              asset: false,
              institution: institution,
              description: description,
              balance: balance,
              interestRate: interestRate,
            })
          }}
        >
          Add
        </Button>
        <Button
          onClick={() => {
            onClose(null);
          }}
        >
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
};

//
export { AssetDialog, LiabilityDialog };
