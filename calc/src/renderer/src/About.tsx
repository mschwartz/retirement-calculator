const About = () => {
  return (
    (
      <>
        <h1>About this Retirement Calculator</h1>
        <p>
          This program is free software that can model retirement plans.
        </p>
        <p>
          It is provided for informational purposes.  There is no guarantee that past results will predict the future.
        </p>
        <p>
          Do not expect a guarantee or waranty for this program.  It is a best effort to model a few possible retirement plans.
        </p>
        <h2 style={{ color: 'red' }}>Use at your own risk!</h2>
        <h2>Features</h2>
        <ul>
          <li>Packaged as an application so you can run this calculator on your computer. </li>
          <li>Data are stored locally, never uploades to the cloud or anywhere else.</li>
          <li>Unlimited brokerage, savings, and retirement accounts.</li>
          <li>Monte Carlo Simulation using historical S&P 500 data (4% rule).</li>
          <li>Alternative Draw Down Strategy.</li>
        </ul>

        <h2>Expenses</h2>
        <p>

        </p>

        <h2>Getting Started</h2>
        <p>
          The Profile tab is where you enter information about yourself; your age, your retirement date, amount of money you are saving each month, monthly expenses, date mortgage is paid off, and so on.
        </p>
        <p>
          The Accounts tab is where you enter information about your savings and retirement accounts.  This is your nest egg, the money you will use to pay expenses during retirement.
        </p>
        <p>
          The Monte Carlo tab is where you can tweak assumptions and run many simulations to see how likely you will not run out of money, using the "4% rule."
          The simulation uses actual S&P 500 data and random start years (each iteration) so your plan is exercised against good and bad stock markets.
        </p>
        <p>
          The Draw Down tab provides an alternate strategy for using your nest egg during retirement.  If you have a decent amount in your accounts, but the 4%
          rule does not provide enough income to pay your expenses, the Draw Down strategy might work out.
        </p>
      </>
    )
  );
};

//
export default About;
