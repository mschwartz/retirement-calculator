import { useState, useCallback } from 'react';

import Paper from '@mui/material/Paper';

import TableContainer from '@mui/material/TableContainer';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';

import Divider from '@mui/material/Divider';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';

import useLocalStorage from './hooks/useLocalStorage';

import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

import { AssetDialog, LiabilityDialog } from './components/AssetLiabilityDialog';
import HelpDialog from './components/HelpDialog';

import { AccountType, AssetType, LiabilityType, liabilityKinds, LiabilityKindType } from './interfaces/AssetLiability';

let nextAccountId = 1;

const help = [
  'An "asset" is something you own that has value. The equity in your home is an asset, your bank and brokerage accounts are assets, the value of your car is an asset, your retirement accounts are assets.',
  'A "liability" is something that you owe, such as a loan.  The amount you owe on your mortgage is a liability, your credit card balances are a liability, your auto loan is a liability, any line of credits you have are liabiliteis',
  'Your net worth is your total assets minus your total liabilities.',
  'This retirement calculator uses your assets to calculate your possible monthly and yearly income from selling these assets over time.',
  'The types of your assets is also used to determine the order for which accounts should be drawn from. For example, tax deferred accounts (401K, Roth, IRA) are drawn from last to allow your investments to grow tax free.',
];

/**
 *
 * @returns
 */
const AssetsAndLiabilities = () => {
  const [showAssetDialog, setShowAssetDialog] = useState(false);
  const [showHelpDialog, setShowHelpDialog] = useState(false);
  const [showLiabilityDialog, setShowLiabilityDialog] = useState(false);
  const [accounts, setAccounts] = useLocalStorage<AccountType[]>('accounts', []);
  const [currentAccount, setCurrentAccount] = useState<AccountType | undefined>(undefined);

  const Summary = () => {
    let count = 0,
      assets = 0,
      property = 0,
      liabilities = 0;

    accounts.map((account) => {
      count++;
      if (account.kind === 'Property') {
        property += account.balance;
      }
      if (account.kind === 'Loan' || account.kind === 'Mortgage') {
        liabilities += account.balance;
      }
      else {
        assets += account.balance;
      }
    });

    const worth = assets - liabilities;
    return (
      <Paper elevation={3} style={{ width: '25%', padding: 20 }}>
        <div style={{ fontSize: '1.2rem', fontWeight: 'bold', marginBottom: 8 }}>Summary</div>
        {renderSummaryItem('Total Accounts', String(count))}
        {renderSummaryItem('Cash & Investments', `$${assets.toLocaleString()}`)}
        {renderSummaryItem('Property Value', `$${property.toLocaleString()}`)}

        <Divider sx={{ mb: 1 }} />
        {renderSummaryItem('Loans/Liabilities', `$${liabilities.toLocaleString()}`)}

        <Divider sx={{ mb: 1 }} />
        {renderSummaryItem('Net Worth', `$${worth.toLocaleString()}`)}
      </Paper>

    )
  };

  console.log('accounts:', accounts)
  const renderSummaryItem = (text: string, value: string) => {
    return (
      <Stack direction="row">
        <div style={{ flexGrow: 1 }}>{text}:</div>
        <div style={{ textAlign: 'right' }}>{value}</div>
      </Stack>
    );
  }
  const addAccount = useCallback((account: AccountType) => {
    if (account.id == null) {
      account.id = nextAccountId++;
    }
    const newAccounts = accounts.filter((a) => {
      return account.id !== a.id;
    });
    newAccounts.push(account);
    setAccounts(newAccounts);
  }, []);

  const removeAccount = useCallback((id: number) => {
    const newAccounts = accounts.filter((a) => {
      return id !== a.id;
    });
    setAccounts(newAccounts);
  }, []);

  let key = 0;

  return (
    <>
      <HelpDialog
        show={showHelpDialog}
        title="About Assets & Liabilities"
        help={help}
        onClose={() => setShowHelpDialog(false)}
      />
      <AssetDialog
        show={showAssetDialog}
        asset={currentAccount as AssetType}
        onClose={(account: AccountType | null) => {
          if (account != null) {
            addAccount(account);
          }
          setShowAssetDialog(false);
        }}
      />
      <LiabilityDialog
        show={showLiabilityDialog}
        liability={currentAccount as LiabilityType}
        onClose={(account: AccountType | null) => {
          if (account != null) {
            addAccount(account);;
          }
          setShowLiabilityDialog(false);
        }}
      />
      <h1>Assets & Liabilites</h1>
      <ButtonGroup
        variant="text"
      >
        <Button
          onClick={() => {
            setCurrentAccount(undefined);
            setShowAssetDialog(true);
          }}
        >
          Add Asset
        </Button>
        <Button
          onClick={() => {
            setCurrentAccount(undefined);
            setShowLiabilityDialog(true);
          }}
        >
          Add Liability
        </Button>

        <Button
          onClick={() => {
            const newAccounts = accounts.filter((account) => {
              return liabilityKinds.indexOf(account.kind as LiabilityKindType) !== -1;
            })
            setAccounts(newAccounts);
          }}
        >
          Clear Assets
        </Button>
        <Button
          onClick={() => {
            const newAccounts = accounts.filter((account) => {
              return liabilityKinds.indexOf(account.kind as LiabilityKindType) === -1;
            })
            setAccounts(newAccounts);
          }}
        >
          Clear Liabilities
        </Button>
        <Button
          onClick={() => {
            setShowHelpDialog(true);
          }}
        >
          Help
        </Button>
      </ButtonGroup>

      <Stack direction="row" spacing={3}>
        <TableContainer style={{ width: '70%' }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Description</TableCell>
                <TableCell>Institution</TableCell>
                <TableCell>Type</TableCell>
                <TableCell>Interest/Appreciation</TableCell>
                <TableCell>Balance</TableCell>
                <TableCell> </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {accounts.map((account) => {
                return (
                  <TableRow key={++key}>
                    <TableCell>
                      <Button
                        onClick={() => {
                          setCurrentAccount(account);
                          setShowAssetDialog(true);
                        }}
                      >
                        {account.description}
                      </Button>
                    </TableCell>
                    <TableCell>{account.institution}</TableCell>
                    <TableCell>{account.kind}</TableCell>
                    <TableCell>{account.interestRate}%</TableCell>
                    <TableCell>{account.asset ? '$' : '-$'}{account.balance.toLocaleString()}</TableCell>
                    <TableCell>
                      <Button
                        onClick={() => {
                          if (account.id != null) {
                            removeAccount(account!.id);
                          }
                        }}
                      >
                        <DeleteForeverIcon />
                      </Button>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        <Summary />
      </Stack>
    </>
  );
};

//
export default AssetsAndLiabilities;
