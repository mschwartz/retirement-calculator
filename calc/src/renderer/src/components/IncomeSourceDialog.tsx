import { useState, useEffect } from 'react';

import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';

import Paper from '@mui/material/Paper';
import ButtonGroup from '@mui/material/ButtonGroup';
import Button from '@mui/material/Button';

import Stack from '@mui/material/Stack';
import Box from '@mui/material/Box';
import FormGroup from '@mui/material/FormGroup';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import InputLabel from '@mui/material/InputLabel';
import TextField from '@mui/material/TextField';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';

import NumericField from '../components/NumericField';

import { IncomeType, IncomeKindType, incomeKinds, IncomeFrequencyKindType, incomeFrequencyKinds } from '../interfaces/IncomeSources';

const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
];

interface IncomeSourceDialogProps {
  show: boolean;
  incomeSource?: IncomeType;
  onClose: Function;
};
const IncomeSourceDialog = ({ show, incomeSource, onClose }: IncomeSourceDialogProps) => {
  const [kind, setKind] = useState<IncomeKindType>(incomeSource?.kind || 'Other Income');
  const [frequency, setFrequency] = useState<IncomeFrequencyKindType>(incomeSource?.frequency || 'Monthly');
  const [description, setDescription] = useState<string>(incomeSource?.description || '');
  const [amount, setAmount] = useState<number | string>(incomeSource?.amount || 0);
  const [error, setError] = useState<string[]>([]);
  const [month, setMonth] = useState<string>('January');

  const close = (value: IncomeType | null) => {
    onClose(value);
  };

  useEffect(() => {
    const newError: string[] = [];
    if (description == '') {
      newError.push('Description is a required field');
    }
    setError(newError);
  }, [description]);

  let key = 0;
  return (
    <Dialog
      open={show}
      onClose={(reason: any) => {
        console.log('reason', reason)
        // close(null);
      }}
    >
      <DialogTitle>{incomeSource != null ? 'Edit Income Source' : 'Add Income Source'}</DialogTitle>
      <DialogContent>
        <Paper sx={{ padding: 2 }}>
          <Box
            component="form"
            sx={{
              '& > :not(style)': { m: 1, width: '25ch' },
            }}
            noValidate
            autoComplete="off"
          >
            <FormGroup>
              <Stack direction="column" spacing={2}>
                <FormControl fullWidth>
                  <InputLabel id="asset-select-label">Income Type</InputLabel>
                  <Select
                    value={kind}
                    label="Income Type"
                    onChange={(event: SelectChangeEvent) => {
                      setKind(event.target.value as IncomeKindType);
                    }}
                  >
                    {incomeKinds.map((s: string) => {
                      return <MenuItem key={++key} value={s}>{s}</MenuItem>;
                    })}
                  </Select>
                </FormControl>
                <TextField
                  label="Account Description"
                  error={description.length === 0}
                  helperText="Description/Account Number"
                  value={description}
                  onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                    setDescription(event.target.value);
                  }}
                />
                <NumericField
                  label="Income Amount"
                  error={typeof amount === 'string'}
                  helperText="Amount of Income"
                  value={amount}
                  onChange={(v: string) => {
                    setAmount(v.length ? Number(v) : '')
                  }}
                />

                <FormControl fullWidth>
                  <InputLabel id="frequency-select-label">Income Frequency</InputLabel>
                  <Select
                    value={frequency}
                    label="Income Frequency"
                    onChange={(event: SelectChangeEvent) => {
                      setFrequency(event.target.value as IncomeFrequencyKindType);
                    }}
                  >
                    {incomeFrequencyKinds.map((s: string) => {
                      return <MenuItem key={++key} value={s}>{s}</MenuItem>;
                    })}
                  </Select>
                </FormControl>

                <FormControl fullWidth>
                  <InputLabel id="month-select-label">Month Income Starts</InputLabel>
                  <Select
                    value={month}
                    label="Month Income Starts"
                    onChange={(event: SelectChangeEvent) => {
                      setMonth(event.target.value as IncomeFrequencyKindType);
                    }}
                  >
                    {months.map((s: string) => {
                      return <MenuItem key={++key} value={s}>{s}</MenuItem>;
                    })}
                  </Select>
                </FormControl>
              </Stack>
            </FormGroup>
          </Box>
        </Paper>
      </DialogContent>
      <DialogActions>
        <ButtonGroup
          variant="text"
        >
          <Button
            onClick={() => onClose(null)}
          >
            Cancel
          </Button>
          <Button
            disabled={error.length > 0}
            onClick={() => {
              onClose({
                kind: kind,
                frequency: frequency,
                description: description,
                amount: amount,
                month: month
              });
            }}
          >
            Accept
          </Button>
        </ButtonGroup>
      </DialogActions>
    </Dialog>
  );
};

//
export default IncomeSourceDialog;
