import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';

interface HelpDialogProps {
  show: boolean;
  title: string;
  help: string[];
  onClose: Function;
};
const HelpDialog = ({ show, title, help, onClose }: HelpDialogProps) => {
  const close = () => {
    onClose();
  }
  let key = 0;
  return (
    <Dialog open={show} onClose={close}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        {help.map((text: string) => {
          return <p key={++key}>{text}</p>
        })}
      </DialogContent>
      <DialogActions>
        <Button
          onClick={close}
        >
          Close
        </Button>
      </DialogActions>
    </Dialog>
  )
};

//
export default HelpDialog;
