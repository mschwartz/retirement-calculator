export const incomeKinds = [
  'Annuity',
  'Pension',
  'Dividend',
  'Interest',
  'Rent',
  'Other Income',
] as const;
export type IncomeKindType = typeof incomeKinds[number];

export const incomeFrequencyKinds = [
  'Daily',
  'Weekly',
  'Bi-Weekly',
  'Monthly',
  'Bi-Monthly',
  'Quarterly',
  'Yearly',
];
export type IncomeFrequencyKindType = typeof incomeFrequencyKinds[number];

export type IncomeType = {
  id?: number;
  kind: IncomeKindType;
  description: string;
  frequency: IncomeFrequencyKindType;
  amount: number;
  month: number; // month that income will be paid (for quarterly dividend, for example)
};
