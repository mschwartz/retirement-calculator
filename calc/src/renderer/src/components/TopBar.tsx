import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

interface TopBarProps {
  chooseTheme: Function;
};
const TopBar = ({ chooseTheme }: TopBarProps) => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed">
        <Toolbar variant="dense">
          <IconButton
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="h6"
            color="inherit"
            component="div"
            sx={{ flexGrow: 1 }}
          >
            Retirement Calculator
          </Typography>
          <Button
            color="inherit"
            onClick={() => {
              chooseTheme();
            }}
          >
            Toggle Theme
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

//
export default TopBar;
