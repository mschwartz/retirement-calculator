export const assetKinds = [
  'Generic Asset', 'Brokerage', 'Savings', 'Checking', 'Debit Card', '401K', 'IRA', 'Roth', 'HSA', 'Property'
] as const;
export type AssetKindType = typeof assetKinds[number];

export const liabilityKinds = [
  'Generic Liability', 'Mortgage', 'Loan', 'Credit Card'
] as const;
export type LiabilityKindType = typeof liabilityKinds[number];

export type AccountKindType = AssetKindType | LiabilityKindType;

export type AssetType = {
  id?: number;
  asset: true;
  kind: AssetKindType;
  institution: string;
  description: string;
  balance: number;
  interestRate: number;
};
export type LiabilityType = {
  id?: number;
  asset: false;
  kind: LiabilityKindType;
  institution: string;
  description: string;
  balance: number;
  interestRate: number;
};
export type AccountType = AssetType | LiabilityType;
