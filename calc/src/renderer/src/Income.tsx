import { useState, useCallback } from 'react';

import ButtonGroup from '@mui/material/ButtonGroup';
import Button from '@mui/material/Button';

import TableContainer from '@mui/material/TableContainer';
import Table from '@mui/material/Table';
import TableHead from '@mui/material/TableHead';
import TableBody from '@mui/material/TableBody';
import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';

import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

import useLocalStorage from './hooks/useLocalStorage';

import HelpDialog from './components/HelpDialog';
import IncomeSourceDialog from './components/IncomeSourceDialog';

import { IncomeType } from './interfaces/IncomeSources';

const help = [
  'Income sources are weekly, bi-weekly, monthly, twice monthly, and/or yearly deposits into your accounts.',
  'These sources include any pensions, rental income, annuities, bond interest, stock dividends, and so on.', ,
  'These sources DO NOT include any Social Security income - that is accounted for elsewhere.',
];
type IncomeSourcesType = {
  nextIncomeSourceId: number;
  incomeSources: IncomeType[];
};
const Income = () => {
  const [sources, setSources] = useLocalStorage<IncomeSourcesType>('income-sources', {
    nextIncomeSourceId: 1,
    incomeSources: []
  });
  const [source, setSource] = useState<IncomeType | undefined>(undefined);
  const [showHelpDialog, setShowHelpDialog] = useState(false);
  const [showIncomeSourceDialog, setShowIncomeSourceDialog] = useState(false);

  const resetSources = () => {
    setSources({
      nextIncomeSourceId: 1,
      incomeSources: []
    });
  };

  console.log('sources', sources);

  const addIncomeSource = useCallback((incomeSource?: IncomeType) => {
    if (incomeSource == null) {
      return;
    }

    console.log('add sources', sources)
    let nextId = sources.nextIncomeSourceId;
    console.log('addIncomeSource', nextId, incomeSource, sources);
    if (incomeSource.id == null) {
      incomeSource.id = nextId++;
    }
    const newSources = sources.incomeSources.filter((a) => {
      console.log('id <=> id', incomeSource.id, '<=>', a.id)
      return incomeSource.id !== a.id;
    });
    console.log('newSources', newSources);
    newSources.push(incomeSource);
    console.log('setSources', {
      nextIncomeSourceId: nextId,
      incomeSources: newSources,
    })
    setSources({
      nextIncomeSourceId: nextId,
      incomeSources: newSources,
    });
  }, [sources]);

  const removeIncomeSource = useCallback((id: number) => {
    const newSources = sources.incomeSources.filter((a) => {
      return id !== a.id;
    });
    setSources({
      nextIncomeSourceId: sources.nextIncomeSourceId,
      incomeSources: newSources,
    })
  }, []);

  let key = 0;
  return (
    <>
      <HelpDialog
        show={showHelpDialog}
        title="About Income Sources"
        help={help as string[]}
        onClose={() => setShowHelpDialog(false)}
      />
      <IncomeSourceDialog
        show={showIncomeSourceDialog}
        incomeSource={source}
        onClose={(newSource) => {
          addIncomeSource(newSource);
          setShowIncomeSourceDialog(false)
        }}
      />
      <h1>Income Sources</h1>
      <ButtonGroup
        variant="text"
      >
        <Button
          onClick={() => {
            setSource(undefined)
            setShowIncomeSourceDialog(true)
          }}
        >
          Add Income Source
        </Button>
        <Button
          onClick={() => {
            resetSources()
          }}
        >
          Clear Income Sources
        </Button>
        <Button
          onClick={() => setShowHelpDialog(true)}
        >
          Help
        </Button>
      </ButtonGroup>

      <TableContainer style={{ width: '70%' }}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Description</TableCell>
              <TableCell>Income Type</TableCell>
              <TableCell>Frequency</TableCell>
              <TableCell>Amount</TableCell>
              <TableCell>Income Month</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {sources.incomeSources.map((src) => {
              return (
                <TableRow key={++key}>
                  <TableCell>
                    <Button
                      onClick={() => {
                        setSource(src);
                        setShowIncomeSourceDialog(true);
                      }}
                    >
                      {src.description}
                    </Button>
                  </TableCell>
                  <TableCell>{src.kind}</TableCell>
                  <TableCell>{src.frequency}</TableCell>
                  <TableCell>${src.amount}</TableCell>
                  <TableCell>{src.frequency === 'Quarterly' ? src.month : ''}</TableCell>
                  <TableCell>
                    <Button
                      onClick={() => {
                        if (src.id != null) {
                          removeIncomeSource(src!.id);
                        }
                      }}
                    >
                      <DeleteForeverIcon />
                    </Button>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

//
export default Income;
