import EventEmitter from './EventEmitter';

class LocalStorage extends EventEmitter {
  keys(): string[] {
    return Object.keys(window.localStorage);
  }

  clear() {
    window.localStorage.clear();
    this.emit('clear');
  }

  removeItem(key: string): void {
    window.localStorage.removeItem(key);
    this.emit('remove', key);
  }

  // NOTE: any is what we really want here.
  getItem(key: string, defaultValue?: any): any {
    const item = window.localStorage.getItem(key);
    if (item == null) {
      return defaultValue === undefined ? null : defaultValue;
    }
    try {
      return JSON.parse(item);
    }
    catch (e) {
      return item;
    }
  }

  // NOTE: any is what we really want here.
  setItem(key: string, value: any): void {
    try {
      window.localStorage.setItem(key, JSON.stringify(value));
    }
    catch (e) {
      window.localStorage.setItem(key, value);
    }

    this.emit(key, value);
    this.emit('change', key, value);
  }

  // NOTE: any is what we really want here.
  listen(key: string, fn: Function) {
    this.on(key, (value: any) => {
      fn(value);
    });
  }
};

//
export default new LocalStorage();

