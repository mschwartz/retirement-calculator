import useLocalStorage from '../hooks/useLocalStorage';

import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';

import About from '../About';
import Profile from '../Profile';
import Assets from '../AssetsAndLiabilities';
import Income from '../Income';
import Expenses from '../Expenses';
import MonteCarlo from '../MonteCarlo';
import DrawDown from '../DrawDown';

const navigation = [
  { label: 'About', el: About },
  { label: 'Profile', el: Profile },
  { label: 'Assets & Liabilities', el: Assets },
  { label: 'Income', el: Income },
  { label: 'Expenses', el: Expenses },
  { label: 'Monte Carlo', el: MonteCarlo },
  { label: 'Draw Down', el: DrawDown },
];
interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
  hide?: boolean;
}

const TabPanel = (props: TabPanelProps) => {
  const { children, value, index, hide = true, ...other } = props;

  if (hide) {
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`navigation-tabpanel-${index}`}
        aria-labelledby={`navigation-tab-${index}`}
        {...other}
      >
        {value === index && <Box>{children}</Box>}
      </div>
    );
  }
  return (
    <div
      role="tabpanel"
      style={{ display: value !== index ? 'none' : undefined }}
      id={`navigation-tabpanel-${index}`}
      aria-labelledby={`navigation-tab-${index}`}
      {...other}
    >
      <Box>{children}</Box>
    </div>
  );
};
const Navigation = () => {
  const [activeTab, setActiveTab] = useLocalStorage<number>('navigation-activeTab', 0);
  const handleChange = (_: React.SyntheticEvent, newValue: number) => {
    setActiveTab(newValue);
  };

  const a11yProps = (index: number) => {
    return {
      id: `navigation-tab-${index}`,
      'aria-controls': `navigation-tabpanel-${index}`,
    };
  };

  let tabKey = -1,
    panelKey = -1;

  return (
    <>
      <Box sx={{ width: '100%' }}>
        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
          <Tabs
            textColor="primary"
            indicatorColor="primary"
            value={activeTab}
            onChange={handleChange}
            aria-label="basic tabs example"
          >
            {navigation.map((detail) => {
              tabKey++;
              return (
                <Tab
                  style={{ color: tabKey === activeTab ? 'secondary' : 'primary' }}
                  key={tabKey}
                  label={detail.label}
                  {...a11yProps(tabKey)}
                />
              )
            })}
          </Tabs>
        </Box>
        {navigation.map((detail) => {
          panelKey++;
          return (
            <TabPanel key={panelKey} index={panelKey} value={activeTab} >
              <Container maxWidth="xl">
                <Paper sx={{ fontSize: '1.2rem', paddingTop: 1, paddingLeft: 3, paddingBottom: 10 }}>
                  <detail.el />
                </Paper>
              </Container>
            </TabPanel>
          )
        })}
      </Box>
    </>
  );
};

export default Navigation;
