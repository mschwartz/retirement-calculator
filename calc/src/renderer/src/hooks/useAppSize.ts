import { useState, useEffect, useRef, useCallback } from 'react';

export interface ScreenSize {
  left: number;
  top: number;
  width: number;
  height: number;
  portrait: boolean;
  topbarHeight: number;
  tabsbarHeight: number;
};

const useAppSize = (): ScreenSize => {
  const initialWidth = window.innerWidth,
    initialHeight = window.innerHeight,
    initialLeft = window.screenLeft,
    initialTop = window.screenTop;
  const mounted = useRef(false);
  const [width, setWidth] = useState<number>(initialWidth);
  const [height, setHeight] = useState<number>(initialHeight);
  const [left, setLeft] = useState<number>(initialLeft);
  const [top, setTop] = useState<number>(initialTop);

  const updateSize = useCallback(() => {
    window.scroll(0, 0);
    if (!mounted.current) {
      return;
    }
    // console.log('window', window)
    setTimeout(() => {
      setWidth(window.innerWidth);
      setHeight(window.innerHeight);
      setLeft(window.screenLeft);
      setTop(window.screenTop);
    }, 100);
  }, []);

  useEffect(() => {
    mounted.current = true;
    updateSize();
    window.addEventListener('resize', updateSize);
    return () => {
      mounted.current = false;
      window.removeEventListener('resize', updateSize);
    };
  }, []);

  return {
    left: left,
    top: top,
    width: width,
    height: height,
    portrait: window.orientation === 0 || window.orientation === 180,
    topbarHeight: 56,
    tabsbarHeight: 42,
  };
};

//
export default useAppSize;

