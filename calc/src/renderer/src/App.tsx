import { useState, useEffect } from 'react';

import CssBaseline from '@mui/material/CssBaseline';

import { ThemeProvider, createTheme } from '@mui/material/styles';

import TopBar from './components/TopBar';
import Navigation from './components/Navigation';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});
const lightTheme = createTheme({
  palette: {
    mode: 'light',
  },
});

function App() {
  const [theme, setTheme] = useState(darkTheme);

  return (
    <>
      <ThemeProvider theme={theme}>
        <CssBaseline enableColorScheme />
        <TopBar
          chooseTheme={() => {
            setTheme(theme === darkTheme ? lightTheme : darkTheme);
          }}
        />
        <div style={{ marginTop: 40 }}>
          <Navigation />
        </div>
      </ThemeProvider >
    </>
  )
}

export default App
