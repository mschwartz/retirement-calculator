type AgeType = {
  months: number,
  days?: number,
  years: number
};

export const getFullRetirementAge = (birthYear: number): AgeType => {
  if (birthYear < 1943) {
    return {
      years: 66,
      days: 0,
      months: 0,
    };
  }
  else if (birthYear <= 1954) {
    return {
      years: 66,
      days: 0,
      months: 2,
    };
  }
  switch (birthYear) {
    case 1955:
      return {
        years: 66,
        days: 0,
        months: 2,
      };
    case 1956:
      return {
        years: 66,
        days: 0,
        months: 4,
      };
    case 1957:
      return {
        years: 66,
        days: 0,
        months: 6,
      };
    case 1958:
      return {
        years: 66,
        days: 0,
        months: 8,
      };
    case 1959:
      return {
        years: 66,
        days: 0,
        months: 10,
      };
    default:
      return {
        years: 67,
        days: 0,
        months: 0,
      };
      break;
  }
};

export const getAge = (dateString): AgeType => {
  var now = new Date();

  var yearNow = now.getFullYear();
  var monthNow = now.getMonth();
  var dateNow = now.getDate();

  var dob = new Date(dateString.substring(6, 10),
    dateString.substring(0, 2) - 1,
    dateString.substring(3, 5)
  );

  var yearDob = dob.getFullYear();
  var monthDob = dob.getMonth();
  var dateDob = dob.getDate();
  var age: any = {};
  var ageString = "";
  var yearString = "";
  var monthString = "";
  var dayString = "";


  var yearAge = yearNow - yearDob;

  if (monthNow >= monthDob)
    var monthAge = monthNow - monthDob;
  else {
    yearAge--;
    var monthAge = 12 + monthNow - monthDob;
  }

  if (dateNow >= dateDob)
    var dateAge = dateNow - dateDob;
  else {
    monthAge--;
    var dateAge = 31 + dateNow - dateDob;

    if (monthAge < 0) {
      monthAge = 11;
      yearAge--;
    }
  }

  age = {
    years: yearAge,
    months: monthAge,
    days: dateAge
  };

  if (age.years > 1) yearString = " years";
  else yearString = " year";
  if (age.months > 1) monthString = " months";
  else monthString = " month";
  if (age.days > 1) dayString = " days";
  else dayString = " day";


  if ((age.years > 0) && (age.months > 0) && (age.days > 0))
    ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString + " old.";
  else if ((age.years == 0) && (age.months == 0) && (age.days > 0))
    ageString = "Only " + age.days + dayString + " old!";
  else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
    ageString = age.years + yearString + " old. Happy Birthday!!";
  else if ((age.years > 0) && (age.months > 0) && (age.days == 0))
    ageString = age.years + yearString + " and " + age.months + monthString + " old.";
  else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
    ageString = age.months + monthString + " and " + age.days + dayString + " old.";
  else if ((age.years > 0) && (age.months == 0) && (age.days > 0))
    ageString = age.years + yearString + " and " + age.days + dayString + " old.";
  else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
    ageString = age.months + monthString + " old.";
  else ageString = "Oops! Could not calculate age!";

  return age;
};
