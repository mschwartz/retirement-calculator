import { useEffect, useState, ChangeEvent } from 'react';

import useLocalStorage from './hooks/useLocalStorage';

import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';

import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs from 'dayjs';

import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';

import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import { TextField } from "@mui/material";
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import Checkbox from '@mui/material/Checkbox';
import { NumberField } from 'mui-number-field';
import { getAge, getFullRetirementAge } from './lib/Utils';

const NumericField = (props: any) => {
  let {
    value,
    min,
    max,
    onChange,
    ...otherProps
  } = props;
  min = min || 0;
  max = max || 110;
  if (value < min || value > max) {
    value = '';
  }

  return (
    <NumberField
      value={value}
      {...otherProps}
      onChange={(event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        if (event.target.value === '') {
          onChange('');
          return;
        }
        const v = Number(event.target.value);
        if (v < min || v > max) {
          return;
        }
        else {
          onChange('' + v);
        }
      }}
    />
  );
};

const Profile = () => {
  const d = new Date().toLocaleDateString('en-US', {
    day: '2-digit',
    month: '2-digit',
    year: 'numeric',
  })

  const defaultBirthDate = d;
  const [married, setMarried] = useLocalStorage<boolean>('marital-status', false);
  // const [married, setMarried] = useState<boolean>(false);
  const [name, setName] = useLocalStorage('primary-name', '');
  const [spouseName, setSpouseName] = useLocalStorage('spouse-name', '');

  const [birthDate, setBirthDate] = useLocalStorage<string>('primary-birthdate', defaultBirthDate);
  const [spouseBirthDate, setSpouseBirthDate] = useLocalStorage<string>('spouse-birthdate', defaultBirthDate);

  const [retirementAge, setRetirementAge] = useLocalStorage<number | string>('primary-retirement-age', 62);
  const [spouseRetirementAge, setSpouseRetirementAge] = useLocalStorage<number | string>('spouse-retirement-age', 62);

  const [fullRetirementAge, setFullRetirementAge] = useLocalStorage<string>('primary-full-retirement-age', '');
  const [spouseFullRetirementAge, setSpouseFullRetirementAge] = useLocalStorage<string>('spouse-full-retirement-age', '');

  const [currentAge, setCurrentAge] = useState<string>('');
  const [spouseCurrentAge, setSpouseCurrentAge] = useState<string>('');

  const [fraBenefit, setFraBenefit] = useLocalStorage<number | string>('fra-benefit', '');
  const [spouseFraBenefit, setSpouseFraBenefit] = useLocalStorage<number | string>('spouse-fra-benefit', '');

  useEffect(() => {
    const newAge = getAge(birthDate);
    const ageString = `${newAge.years} years, ${newAge.months} months`;
    setCurrentAge(ageString)
    const fd = new Date(birthDate);
    const fra = getFullRetirementAge(fd.getFullYear())
    if (fra.months === 0) {
      setFullRetirementAge(`Age ${fra.years}`);
    }
    else {
      setFullRetirementAge(`Age ${fra.years} years and ${fra.months} months`);
    }
  }, [birthDate]);

  useEffect(() => {
    const newAge = getAge(spouseBirthDate);
    const ageString = `${newAge.years} years, ${newAge.months} months`;
    setSpouseCurrentAge(ageString)
    const fd = new Date(spouseBirthDate);
    const fra = getFullRetirementAge(fd.getFullYear())
    if (fra.months == 0) {
      setSpouseFullRetirementAge(`Age ${fra.years}`);
    }
    else {
      setSpouseFullRetirementAge(`Age ${fra.years} years and ${fra.months} months`);
    }
  }, [spouseBirthDate]);

  return (
    <>
      <h1>Your Profile</h1>

      <Stack direction="row">
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox
                checked={married}
                onChange={() => {
                  setMarried(!married)
                }}
              />
            }
            label="Include Spouse"
          />
        </FormGroup>
        <Button>
          Reset to default values
        </Button>
      </Stack>

      <Box
        component="form"
        sx={{
          '& .MuiTextField-root': { m: 1, width: '25ch' },
        }}
        noValidate
        autoComplete="off"
      >
        <Stack direction="row" spacing={4}>
          <div style={{ width: '33%' }}>
            <h3>About You</h3>
            <div>
              <TextField
                label="Your First Name"
                value={name}
                onChange={(e: any) => {
                  setName(e.target.value);
                }}
              />
            </div>
            <div>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DatePicker
                  label="Your Birth Date"
                  value={dayjs(birthDate)}
                  onChange={(e: any) => {
                    const d = e.$d.toLocaleDateString('en-US', {
                      day: '2-digit',
                      month: '2-digit',
                      year: 'numeric',
                    })
                    setBirthDate(d);
                  }}
                />
              </LocalizationProvider>
              <TextField
                disabled
                label="Your Current Age"
                value={currentAge}
              />
            </div>
            <div>
              <NumericField
                label="Age You Will Retire"
                value={retirementAge}
                onChange={(v: string) => {
                  setRetirementAge(v.length ? Number(v) : '')
                }}
              />
            </div>
            <div>
              <TextField
                disabled
                label="Your Full Retirement Age"
                value={fullRetirementAge}
              />
            </div>
            <div>
              <NumericField
                label="Social Security Benefit at FRA"
                min={1}
                max={5000}
                value={fraBenefit}
                onChange={(v: string) => {
                  setFraBenefit(v.length ? Number(v) : '')
                }}
              />
            </div>
          </div>

          {married ?
            <div style={{ width: '33%' }}>
              <h3>About your spouse</h3>
              <div>
                <TextField
                  label="Spouse's First Name"
                  value={spouseName}
                  onChange={(e: any) => {
                    setSpouseName(e.target.value);
                  }}
                />
              </div>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DatePicker
                  label="Spouse's Birth Date"
                  value={dayjs(spouseBirthDate)}
                  onChange={(e: any) => {
                    const d = e.$d.toLocaleDateString('en-US', {
                      day: '2-digit',
                      month: '2-digit',
                      year: 'numeric',
                    })
                    setSpouseBirthDate(d);
                  }}
                />
              </LocalizationProvider>
              <div>
                <TextField
                  disabled
                  label="Spouse's Current Age"
                  value={spouseCurrentAge}
                />
              </div>
              <div>
                <NumericField
                  label="Age Your Spouse Will Retire"
                  value={spouseRetirementAge}
                  onChange={(v: string) => {
                    setSpouseRetirementAge(v.length ? Number(v) : '')
                  }}
                />
              </div>
              <div>
                <TextField
                  disabled
                  label="Spouse's Full Retirement Age"
                  value={spouseFullRetirementAge}
                />
              </div>
              <div>
                <NumericField
                  label="Spouse's Social Security Benefit at FRA"
                  min={1}
                  max={5000}
                  value={spouseFraBenefit}
                  onChange={(v: string) => {
                    setSpouseFraBenefit(v.length ? Number(v) : '')
                  }}
                />
              </div>
            </div>
            :
            <div style={{ width: '33%' }}>
              <h3>No Spouse</h3>
              <p>
                Spouse information is not used to generate results.  To include your spouse's information, check the "include Spouse" checkbox above.
              </p>
            </div>
          }
          <div style={{ width: '33%', paddingRight: 20 }}>
            <h3>Instructions</h3>
            <p>
              Full Rretirement Age (FRA) for Social Security is the age at which a person is entitled to 100% of their monthly Social Security retirement benefit. It ranges from 66 to 67. The Social Security Administration and this app determines a person’s full retirement age based on their birth year.
            </p>
            <p>
              You can find your FRA monthly Social Security benefit at the Social Security Administration website.
              BE SURE TO ENTER THE AMOUNT FOR YOUR FRA.
            </p>
          </div>
        </Stack>
      </Box >
    </>
  );
};

//
export default Profile;
