import { ChangeEvent } from 'react';
import { NumberField } from 'mui-number-field';

interface NumericFieldProps {
  value: number | string;
  label?: string;
  min?: number;
  max?: number;
  onChange: Function;
  otherProps?: any;
  error?: boolean;
  helperText?: string;
};
const NumericField = ({ value, label, min, max, onChange, error, helperText, otherProps }: NumericFieldProps) => {
  min = min ?? 0;
  max = max ?? Number.MAX_SAFE_INTEGER;

  if (typeof value !== 'number' || value < min || value > max) {
    value = '';
  }

  return (
    <NumberField
      label={label}
      error={error}
      helperText={helperText}
      value={value}
      {...otherProps}
      onChange={(event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        min = min ?? 0;
        max = max ?? Number.MAX_SAFE_INTEGER;
        if (event.target.value === '') {
          onChange('');
          return;
        }
        const v = Number(event.target.value);
        if (v < min || v > max) {
          return;
        }
        else {
          onChange('' + v);
        }
      }}
    />
  );
};

//
export default NumericField;
