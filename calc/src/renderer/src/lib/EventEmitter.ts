type ListenerType = {
  [event: string]: Function[]
};

class EventEmitter {
  listeners: ListenerType = {};
  constructor() {
    this.listeners = {};
  }

  setMaxListeners(num: number) {
    console.log("setMaxListeners", num);
  }

  on(event: string, handler: Function) {
    this.listeners[event] = this.listeners[event] || [];
    if (this.listeners[event].indexOf(handler) === -1) {
      this.listeners[event].push(handler);
    }
  }

  un(event: string, handler: Function) {
    this.removeListener(event, handler);
  }

  removeListener(event: string, handler: Function) {
    if (!this.listeners[event]) {
      //      console.error("no listener(s) for event ", event);
      return;
    }
    const newListeners = [];
    for (const listener of this.listeners[event]) {
      if (listener !== handler) {
        newListeners.push(listener);
      }
    }
    this.listeners[event] = newListeners;
  }

  // NOTE: any is what we really want here.
  emit(event: string, ...rest: any) {
    const listeners = this.listeners[event] || [];
    for (const handler of listeners) {
      handler(...rest);
    }
  }

  listenerCount(event: string) {
    const listeners = this.listeners[event] || [];
    return listeners.length;
  }
}

//
export default EventEmitter;
